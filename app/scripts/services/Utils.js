angular.module('protonchat').factory('Utils', [function () {
    function getHttpConfig(method, url, data) {
        var headers = {
            'Content-Type': 'application/json;charset=utf-8'
        };
        var config = {
            method: method,
            url: url,
            data: data,
            headers: headers
        };

        return config;
    }

    function getApiRoot(){
        return "http://api.protonmail.com/";
    }
    return {
        getHttpConfig: getHttpConfig,
        getApiRoot: getApiRoot
    };

}]);
