angular.module('protonchat').factory('User', [function () {
    /*
    Returns users
     */
    function getUsers() {
        var deferred = $q.defer();
        $http.get('../../data/users.json').success(function(data){
            deferred.resolve(data);
        });
        return deferred.promise;
    }

    return {
        getUsers: getUsers
    };
}]);
