angular.module('protonchat').factory('Message', ['API', 'Session', function (API, Session) {
    function postMessage(channelId, messages) {
        // Here we encrypt the message with all public key from users in the channel
        var message = {
            "id": Math.round(Math.random() * 1000),
            "message": messages,
            "user-id": Session.getUser().id,
            "channel-id": "1",
            "time": new Date().getTime()
        };
        console.log(message);
        API.postMessage(channelId, message)
    }

    return {
        postMessage: postMessage
    };
}]);
